Source: tiledb
Priority: optional
Maintainer: Dirk Eddelbuettel <edd@debian.org>
Build-Depends: debhelper-compat (= 13),
 chrpath,
 cmake (>= 3.3~),
 pkg-config,
 capnproto (>= 0.8.0),
 libbz2-dev,
 libcapnp-dev (>= 0.8.0),
 libcurl4-openssl-dev,
 liblz4-dev,
 libmagic-dev,
 libspdlog-dev,
 libssl-dev,
 libzstd-dev,
 zlib1g-dev
Standards-Version: 4.6.2
Section: libs
Homepage: https://tiledb.com/
Vcs-Browser: https://salsa.debian.org/debian/tiledb
Vcs-Git: https://salsa.debian.org/debian/tiledb.git
Rules-Requires-Root: no

Package: libtiledb-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: libtiledb2.13 (= ${binary:Version}), ${misc:Depends}
Suggests: libtiledb-doc
Description: Store and access very large multi-dimensional array data (development headers)
 TileDB allows you to store and access very large multi-dimensional array
 data, the data currency of Data Science.
 .
 It is a powerful storage engine that introduces a novel format that can
 effectively store both dense and sparse array data with support for fast
 updates and reads.
 .
 This package contains the development headers.

Package: libtiledb2.13
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends}
Suggests: libtiledb-doc
Description: Store and access very large multi-dimensional array data (runtime library)
 TileDB allows you to store and access very large multi-dimensional array
 data, the data currency of Data Science.
 .
 It is a powerful storage engine that introduces a novel format that can
 effectively store both dense and sparse array data with support for fast
 updates and reads.
 .
 This package contains the runtime library.

